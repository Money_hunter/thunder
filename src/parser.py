from binance_chain.http import HttpApiClient
from binance_chain.constants import KlineInterval
from binance_chain.environment import BinanceEnvironment

import time
import swagger_client
from swagger_client.rest import ApiException


def parse_market():
    api_instance = swagger_client.MarketApi()
    offset = 0
    BUSD = 'BUSD-BD1'
    BNB = 'BNB'
    bnb_pairs = []
    busd_pairs = []
    lot = dict()
    try:
        print("parsing market pairs")
        while True:
            # Get market pairs.
            # print("offset %d" % offset)
            x = api_instance.get_pairs(limit=500, offset=offset)
            for pair in x:
                alpha = pair["base_asset_symbol"]
                beta = pair["quote_asset_symbol"]
                lot_size = pair["lot_size"]
                if beta == BNB:
                    bnb_pairs.append(alpha)
                elif beta == BUSD:
                    busd_pairs.append(alpha)
                lot[alpha] = lot_size
            offset += 500
            time.sleep(1.5)
    except ApiException as e:
        if e.reason == "Bad Request":
            print("parsing finished")
        else:
            print("Exception when calling MarketApi->getPairs: %s\n" % e)
    bnb_set = set(bnb_pairs)
    intersection = bnb_set.intersection(busd_pairs)
    pairs = dict()
    for item in intersection:
        pairs[item] = lot[item]
    pairs[BNB] = lot[BNB]
    pairs[BUSD] = lot[BUSD]
    return pairs

