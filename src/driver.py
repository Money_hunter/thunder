import sys

sys.path.append("..")
from src import Hunter
import argparse

# Instantiate the parser
parser = argparse.ArgumentParser(description='Rune Arbitrager')
parser.add_argument("-k", "--key", type=str, required=True)
parser.add_argument("-t", "--token", type=str, required=True)
parser.add_argument("-b", "--basesize", type=int, required=True)
parser.add_argument("-o", "--omega", type=int, required=True)

args = parser.parse_args()
x = Hunter(args.key, args.basesize, args.omega)
# token = "RUNE-B1A"
x.hunt(args.token)
