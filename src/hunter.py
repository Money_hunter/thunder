import sys

sys.path.append("..")
import time
import swagger_client
import argparse
from swagger_client.rest import ApiException
from binance_chain.http import HttpApiClient
from binance_chain.messages import NewOrderMsg
from binance_chain.wallet import Wallet
from binance_chain.environment import BinanceEnvironment
from binance_chain.exceptions import BinanceChainAPIException
from src import Monitor, LiquidityError
from src import Logger


class PartialOrderError(Exception):
    pass


class Hunter:
    def __init__(self, key, base_size, omega):
        self.key = key
        self.base_size = base_size
        self.omega = omega
        self.env = BinanceEnvironment()
        self.wallet = Wallet(private_key=key, env=self.env)
        self.client = HttpApiClient(env=self.env)
        self.monitor = Monitor(self.wallet, self.base_size, self.omega)
        print("Markets: ")
        self.monitor.print_pairs()
        print("Wallet initialized: ")
        print("address: " + self.wallet.address)

    def hunt(self, token):
        rate = 1.5
        f = open('debug.txt', 'a')
        # logger = Logger(self.tokens, self.wallet.address)
        while True:
            try:
                profit = self.monitor.scan(token)
                if profit and profit > 0:
                    res_alpha = self.client.broadcast_msg(msg=self.monitor.alpha_msg, sync=True)
                    res_beta = self.client.broadcast_msg(msg=self.monitor.beta_msg, sync=True)
                    res_theta = self.client.broadcast_msg(msg=self.monitor.theta_msg, sync=True)
                    # id_alpha = res_alpha[0]['data'][13:-2]
                    # id_beta = res_beta[0]['data'][13:-2]
                    # id_theta = res_theta[0]['data'][13:-2]
                    # f.write(id_alpha)
                    # f.write(id_beta)
                    # f.write(id_theta)
                    # if not logger.log_new_trade(id_alpha):
                    #     raise PartialOrderError("Alpha failed")
                    # if not logger.log_new_trade(id_beta):
                    #     raise PartialOrderError("Beta failed")
                    # if not logger.log_new_trade(id_theta):
                    #     raise PartialOrderError("Theta failed")
                    # logger.log_pnl(self.tokens[2])
                time.sleep(1/rate)
            except LiquidityError as e:
                print("Liquidity Issue on pair %s\n" % e)
                time.sleep(3)
            except PartialOrderError as f:
                print("%s\n" % f)
                time.sleep(1)
            except BinanceChainAPIException as unsolved:
                f.write(str(unsolved))
                f.write('\n')
        f.close()


