import time
import swagger_client
from decimal import Decimal, ROUND_DOWN
from swagger_client.rest import ApiException
from binance_chain.messages import NewOrderMsg


class LiquidityError(Exception):
    pass


class Monitor:
    def __init__(self, wallet, base_size, omega):
        self.BUSD = 'BUSD-BD1'
        self.BNB = 'BNB'
        self.pairs = dict()
        self.api_instance = swagger_client.MarketApi()
        self.parse_market()
        self.wallet = wallet
        self.base_size = base_size
        self.fee = 0.0004
        self.omega = omega
        self.alpha_msg = None
        self.beta_msg = None
        self.theta_msg = None
        self.bnb_lot_size = self.pairs[self.BNB]
        print("Monitor initialized")

    def parse_market(self):
        offset = 0
        bnb_pairs = []
        busd_pairs = []
        lot = dict()
        try:
            print("parsing market pairs")
            while True:
                # Get market pairs.
                # print("offset %d" % offset)
                x = self.api_instance.get_pairs(limit=500, offset=offset)
                for pair in x:
                    alpha = pair["base_asset_symbol"]
                    beta = pair["quote_asset_symbol"]
                    lot_size = pair["lot_size"]
                    if beta == self.BNB:
                        bnb_pairs.append(alpha)
                    elif beta == self.BUSD:
                        busd_pairs.append(alpha)
                    lot[alpha] = lot_size
                offset += 500
                time.sleep(1.5)
        except ApiException as e:
            if e.reason == "Bad Request":
                print("parsing finished")
            else:
                print("Exception when calling MarketApi->getPairs: %s\n" % e)
        bnb_set = set(bnb_pairs)
        intersection = bnb_set.intersection(busd_pairs)
        for item in intersection:
            self.pairs[item] = lot[item]
        self.pairs[self.BNB] = lot[self.BNB]
        self.pairs[self.BUSD] = lot[self.BUSD]

    def print_pairs(self):
        for key, value in self.pairs.items():
            print(key, ' : ', value)

    ''' Scan and construct message for broadcast '''
    def scan(self, token):
        try:
            # Strategy for BUSD gain, buy bnb, buy rune, sell rune
            print("Monitoring ---")
            # Get the order book.
            # api_response.asks | api_response.bids
            busd_pair = token + '_' + self.BUSD
            bnb_pair = token + '_' + self.BNB
            bnb_busd_pair = self.BNB + '_' + self.BUSD
            token_busd = self.api_instance.get_depth(busd_pair, limit=5)
            token_bnb = self.api_instance.get_depth(bnb_pair, limit=5)
            bnb_busd = self.api_instance.get_depth(bnb_busd_pair, limit=5)

            # route 1: buy token, sell to bnb, sell to busd
            token_in = Decimal(self.base_size)
            busd_out = token_in * Decimal(token_busd.asks[0][0])
            bnb_in = token_in * Decimal(token_bnb.bids[0][0])
            bnb_out = bnb_in.quantize(
                Decimal(self.bnb_lot_size.strip('0')), rounding=ROUND_DOWN)
            busd_in = bnb_out * Decimal(bnb_busd.bids[0][0])
            leftover_bnb = bnb_in - bnb_out
            profit_a = busd_in - busd_out * Decimal(1 + self.fee * 3) + leftover_bnb * Decimal(0.5) * Decimal(bnb_busd.bids[0][0])

            # route 2: buy bnb, buy token, sell to busd
            busd_out_2 = bnb_out * Decimal(bnb_busd.asks[0][0])
            bnb_in_2 = bnb_out
            token_in_2 =  bnb_in_2 / Decimal(token_bnb.asks[0][0])
            token_out_2 = token_in_2.quantize(
                Decimal(self.pairs[token].strip('0')), rounding=ROUND_DOWN)
            busd_in_2 = token_out_2 * Decimal(token_busd.bids[0][0])
            leftover_rune = token_in_2 - token_out_2
            profit_b = busd_in_2 - busd_out_2 * Decimal(1 + self.fee * 3)

            print("route 1: %s" % profit_a)
            print("route 2: %s" % profit_b)
            if profit_a > 0:
                alpha_a_liquidity = Decimal(token_busd.asks[0][1]) >= token_in * self.omega
                beta_a_liquidity = Decimal(token_bnb.bids[0][1]) >= token_in * self.omega
                theta_a_liquidity = Decimal(bnb_busd.bids[0][1]) >= bnb_out * self.omega
                if alpha_a_liquidity and beta_a_liquidity and theta_a_liquidity:
                    self.alpha_msg = NewOrderMsg(
                        wallet=self.wallet,
                        symbol=busd_pair,
                        time_in_force=3,  # 1 for Good Till Expire(GTE) order and 3 for Immediate Or Cancel (IOC)
                        order_type=2,  # only accept 2 for now, meaning limit order
                        side=1,  # 1 for buy and 2 for sell
                        price=Decimal(token_busd.asks[0][0]),
                        quantity=token_in,
                    )
                    self.beta_msg = NewOrderMsg(
                        wallet=self.wallet,
                        symbol=bnb_pair,
                        time_in_force=3,  # 1 for Good Till Expire(GTE) order and 3 for Immediate Or Cancel (IOC)
                        order_type=2,  # only accept 2 for now, meaning limit order
                        side=2,  # 1 for buy and 2 for sell
                        price=Decimal(token_bnb.bids[0][0]),
                        quantity=token_in,
                    )
                    self.theta_msg = NewOrderMsg(
                        wallet=self.wallet,
                        symbol=bnb_busd_pair,
                        time_in_force=3,  # 1 for Good Till Expire(GTE) order and 3 for Immediate Or Cancel (IOC)
                        order_type=2,  # only accept 2 for now, meaning limit order
                        side=2,  # 1 for buy and 2 for sell
                        price=Decimal(bnb_busd.bids[0][0]),
                        quantity=bnb_out,
                    )
                    return profit_a
                elif profit_b <= 0:
                    raise LiquidityError("Route 1 no liquidity")
            elif profit_b > 0:
                alpha_b_liquidity = Decimal(bnb_busd.asks[0][1]) >= bnb_in_2 * self.omega
                beta_b_liquidity = Decimal(token_bnb.bids[0][1]) >= token_out_2 * self.omega
                theta_b_liquidity = Decimal(token_busd.bids[0][1]) >= token_out_2 * self.omega
                if alpha_b_liquidity and beta_b_liquidity and theta_b_liquidity:
                    self.alpha_msg = NewOrderMsg(
                        wallet=self.wallet,
                        symbol=bnb_busd_pair,
                        time_in_force=3,  # 1 for Good Till Expire(GTE) order and 3 for Immediate Or Cancel (IOC)
                        order_type=2,  # only accept 2 for now, meaning limit order
                        side=1,  # 1 for buy and 2 for sell
                        price=Decimal(bnb_busd.asks[0][0]),
                        quantity=bnb_in_2,
                    )
                    # Construct Message for Beta Order
                    self.beta_msg = NewOrderMsg(
                        wallet=self.wallet,
                        symbol=bnb_pair,
                        time_in_force=3,  # 1 for Good Till Expire(GTE) order and 3 for Immediate Or Cancel (IOC)
                        order_type=2,  # only accept 2 for now, meaning limit order
                        side=1,  # 1 for buy and 2 for sell
                        price=Decimal(token_bnb.asks[0][0]),
                        quantity=token_out_2,
                    )
                    # Construct Message for Theta Order
                    self.theta_msg = NewOrderMsg(
                        wallet=self.wallet,
                        symbol=busd_pair,
                        time_in_force=3,  # 1 for Good Till Expire(GTE) order and 3 for Immediate Or Cancel (IOC)
                        order_type=2,  # only accept 2 for now, meaning limit order
                        side=2,  # 1 for buy and 2 for sell
                        price=Decimal(token_busd.bids[0][0]),
                        quantity=token_out_2,
                    )
                    return profit_b
                else:
                    raise LiquidityError("Route 2 no liquidity")
        except ApiException as e:
            print("Exception when calling MarketApi->getDepth: %s\n" % e)
            return 0

    ''' Test function that takes json input'''
    def test_scan(self, token, token_busd, token_bnb, bnb_busd):
        busd_pair = token + '-' + self.BUSD
        bnb_pair = token + '-' + self.BNB
        bnb_busd_pair = self.BNB + '-' + self.BUSD
        # route 1: buy token, sell to bnb, sell to busd
        token_in = Decimal(self.base_size)
        busd_out = token_in * Decimal(token_busd['asks'][0][0])
        bnb_in = token_in * Decimal(token_bnb['bids'][0][0])
        bnb_out = bnb_in.quantize(
            Decimal(self.bnb_lot_size.strip('0')), rounding=ROUND_DOWN)
        busd_in = bnb_out * Decimal(bnb_busd['bids'][0][0])
        leftover_bnb = bnb_in - bnb_out
        profit_a = busd_in - busd_out * Decimal(1 + self.fee * 3) + leftover_bnb * Decimal(0.5) * Decimal(bnb_busd['bids'][0][0])

        # route 2: buy bnb, buy token, sell to busd
        busd_out_2 = bnb_out * Decimal(bnb_busd['asks'][0][0])
        bnb_in_2 = bnb_out
        token_in_2 = bnb_in_2 / Decimal(token_bnb['asks'][0][0])
        token_out_2 = token_in_2.quantize(
            Decimal(self.pairs[token].strip('0')), rounding=ROUND_DOWN)
        busd_in_2 = token_out_2 * Decimal(token_busd['bids'][0][0])
        leftover_rune = token_in_2 - token_out_2
        profit_b = busd_in_2 - busd_out_2 * Decimal(1 + self.fee * 3)

        if profit_a > 0:
            alpha_a_liquidity = Decimal(token_busd['asks'][0][1]) >= token_in * self.omega
            beta_a_liquidity = Decimal(token_bnb['bids'][0][1]) >= token_in * self.omega
            theta_a_liquidity = Decimal(bnb_busd['bids'][0][1]) >= bnb_out * self.omega
            if alpha_a_liquidity and beta_a_liquidity and theta_a_liquidity:
                self.alpha_msg = NewOrderMsg(
                    wallet=self.wallet,
                    symbol=busd_pair,
                    time_in_force=3,  # 1 for Good Till Expire(GTE) order and 3 for Immediate Or Cancel (IOC)
                    order_type=2,  # only accept 2 for now, meaning limit order
                    side=1,  # 1 for buy and 2 for sell
                    price=Decimal(token_busd['asks'][0][0]),
                    quantity=token_in,
                )
                self.beta_msg = NewOrderMsg(
                    wallet=self.wallet,
                    symbol=bnb_pair,
                    time_in_force=3,  # 1 for Good Till Expire(GTE) order and 3 for Immediate Or Cancel (IOC)
                    order_type=2,  # only accept 2 for now, meaning limit order
                    side=2,  # 1 for buy and 2 for sell
                    price=Decimal(token_bnb['bids'][0][0]),
                    quantity=token_in,
                )
                self.theta_msg = NewOrderMsg(
                    wallet=self.wallet,
                    symbol=bnb_busd_pair,
                    time_in_force=3,  # 1 for Good Till Expire(GTE) order and 3 for Immediate Or Cancel (IOC)
                    order_type=2,  # only accept 2 for now, meaning limit order
                    side=2,  # 1 for buy and 2 for sell
                    price=Decimal(bnb_busd['bids'][0][0]),
                    quantity=bnb_out,
                )
                return profit_a
            elif profit_b <= 0:
                raise LiquidityError("Route 1 no liquidity")
        elif profit_b > 0:
            alpha_b_liquidity = Decimal(bnb_busd['asks'][0][1]) >= bnb_in_2 * self.omega
            beta_b_liquidity = Decimal(token_bnb['bids'][0][1]) >= token_out_2 * self.omega
            theta_b_liquidity = Decimal(token_busd['bids'][0][1]) >= token_out_2 * self.omega
            if alpha_b_liquidity and beta_b_liquidity and theta_b_liquidity:
                self.alpha_msg = NewOrderMsg(
                    wallet=self.wallet,
                    symbol=bnb_busd_pair,
                    time_in_force=3,  # 1 for Good Till Expire(GTE) order and 3 for Immediate Or Cancel (IOC)
                    order_type=2,  # only accept 2 for now, meaning limit order
                    side=1,  # 1 for buy and 2 for sell
                    price=Decimal(bnb_busd['asks'][0][0]),
                    quantity=bnb_in_2,
                )
                # Construct Message for Beta Order
                self.beta_msg = NewOrderMsg(
                    wallet=self.wallet,
                    symbol=bnb_pair,
                    time_in_force=3,  # 1 for Good Till Expire(GTE) order and 3 for Immediate Or Cancel (IOC)
                    order_type=2,  # only accept 2 for now, meaning limit order
                    side=1,  # 1 for buy and 2 for sell
                    price=Decimal(token_bnb['asks'][0][0]),
                    quantity=token_out_2,
                )
                # Construct Message for Theta Order
                self.theta_msg = NewOrderMsg(
                    wallet=self.wallet,
                    symbol=busd_pair,
                    time_in_force=3,  # 1 for Good Till Expire(GTE) order and 3 for Immediate Or Cancel (IOC)
                    order_type=2,  # only accept 2 for now, meaning limit order
                    side=2,  # 1 for buy and 2 for sell
                    price=Decimal(token_busd['bids'][0][0]),
                    quantity=token_out_2,
                )
                return profit_b
            else:
                raise LiquidityError("Route 2 no liquidity")
        else:
            print("route 1 %s" % profit_a)
            print("route 2 %s" % profit_b)
            raise LiquidityError("No Trade")