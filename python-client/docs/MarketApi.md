# swagger_client.MarketApi

All URIs are relative to *https://dex.binance.org/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_depth**](MarketApi.md#get_depth) | **GET** /depth | Get the order book.
[**get_pairs**](MarketApi.md#get_pairs) | **GET** /markets | Get market pairs.


# **get_depth**
> MarketDepth get_depth(symbol, limit=limit)

Get the order book.

Gets the order book depth data for a given pair symbol.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.MarketApi()
symbol = 'symbol_example' # str | Market pair symbol,
limit = 56 # int | The limit of results. Allowed limits: [5, 10, 20, 50, 100, 500, 1000] (optional)

try:
    # Get the order book.
    api_response = api_instance.get_depth(symbol, limit=limit)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MarketApi->get_depth: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **symbol** | **str**| Market pair symbol, | 
 **limit** | **int**| The limit of results. Allowed limits: [5, 10, 20, 50, 100, 500, 1000] | [optional] 

### Return type

[**MarketDepth**](MarketDepth.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_pairs**
> Pairs get_pairs(limit=limit, offset=offset)

Get market pairs.

Gets the list of market pairs that have been listed.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.MarketApi()
limit = 56 # int | how many pairs to ask for (optional)
offset = 56 # int | offset (optional)

try:
    # Get market pairs.
    api_response = api_instance.get_pairs(limit=limit, offset=offset)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MarketApi->get_pairs: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| how many pairs to ask for | [optional] 
 **offset** | **int**| offset | [optional] 

### Return type

[**Pairs**](Pairs.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

