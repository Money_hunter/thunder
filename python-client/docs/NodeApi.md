# swagger_client.NodeApi

All URIs are relative to *https://dex.binance.org/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**broadcast**](NodeApi.md#broadcast) | **POST** /broadcast | Broadcast a transaction.
[**get_peers**](NodeApi.md#get_peers) | **GET** /peers | Get network peers.
[**get_validators**](NodeApi.md#get_validators) | **GET** /validators | Get network peers.


# **broadcast**
> AccountMeta broadcast(body, sync)

Broadcast a transaction.

Broadcasts a signed transaction. A single transaction must be sent hex-encoded with a content-type of text/plain.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.NodeApi()
body = 'body_example' # str | signed message: string (binary) minLength:1 maxLength:100000
sync = true # bool | Synchronous broadcast

try:
    # Broadcast a transaction.
    api_response = api_instance.broadcast(body, sync)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling NodeApi->broadcast: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **str**| signed message: string (binary) minLength:1 maxLength:100000 | 
 **sync** | **bool**| Synchronous broadcast | 

### Return type

[**AccountMeta**](AccountMeta.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_peers**
> AccountMeta get_peers()

Get network peers.

Gets the list of network peers.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.NodeApi()

try:
    # Get network peers.
    api_response = api_instance.get_peers()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling NodeApi->get_peers: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AccountMeta**](AccountMeta.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_validators**
> AccountMeta get_validators()

Get network peers.

Gets the list of network peers.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.NodeApi()

try:
    # Get network peers.
    api_response = api_instance.get_validators()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling NodeApi->get_validators: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AccountMeta**](AccountMeta.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

