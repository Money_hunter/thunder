# MarketDepth

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asks** | **list[list[str]]** |  | [optional] 
**bids** | **list[list[str]]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


