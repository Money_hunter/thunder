# BlockRewards

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**block_reward** | **str** |  | [optional] 
**bond_reward** | **str** |  | [optional] 
**stake_reward** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

