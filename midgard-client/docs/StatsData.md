# StatsData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**daily_active_users** | **str** | Daily active users (unique addresses interacting) | [optional] 
**monthly_active_users** | **str** | Monthly active users | [optional] 
**total_users** | **str** | Total unique swappers &amp; stakers | [optional] 
**daily_tx** | **str** | Daily transactions | [optional] 
**monthly_tx** | **str** | Monthly transactions | [optional] 
**total_tx** | **str** | Total transactions | [optional] 
**total_volume24hr** | **str** | Total (in RUNE Value) of all assets swapped in 24hrs | [optional] 
**total_volume** | **str** | Total (in RUNE Value) of all assets swapped since start. | [optional] 
**total_staked** | **str** | Total staked (in RUNE Value). | [optional] 
**total_depth** | **str** | Total RUNE balances | [optional] 
**total_earned** | **str** | Total earned (in RUNE Value). | [optional] 
**pool_count** | **str** | Number of active pools | [optional] 
**total_asset_buys** | **str** | Total buying transactions | [optional] 
**total_asset_sells** | **str** | Total selling transactions | [optional] 
**total_stake_tx** | **str** | Total staking transactions | [optional] 
**total_withdraw_tx** | **str** | Total withdrawing transactions | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

