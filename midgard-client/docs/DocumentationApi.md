# swagger_client.DocumentationApi

All URIs are relative to *http://18.159.173.48:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_docs**](DocumentationApi.md#get_docs) | **GET** /v1/doc | Get Documents

# **get_docs**
> get_docs()

Get Documents

Swagger/openapi 3.0 specification generated documents.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DocumentationApi()

try:
    # Get Documents
    api_instance.get_docs()
except ApiException as e:
    print("Exception when calling DocumentationApi->get_docs: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

