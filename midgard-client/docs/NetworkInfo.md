# NetworkInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bond_metrics** | [**BondMetrics**](BondMetrics.md) |  | [optional] 
**active_bonds** | **list[str]** | Array of Active Bonds | [optional] 
**standby_bonds** | **list[str]** | Array of Standby Bonds | [optional] 
**total_staked** | **str** | Total Rune Staked in Pools | [optional] 
**active_node_count** | **int** | Number of Active Nodes | [optional] 
**standby_node_count** | **int** | Number of Standby Nodes | [optional] 
**total_reserve** | **str** | Total left in Reserve | [optional] 
**pool_share_factor** | **str** |  | [optional] 
**block_rewards** | [**BlockRewards**](BlockRewards.md) |  | [optional] 
**bonding_roi** | **str** |  | [optional] 
**staking_roi** | **str** |  | [optional] 
**next_churn_height** | **str** |  | [optional] 
**pool_activation_countdown** | **int** | The remaining time of pool activation (in blocks) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

