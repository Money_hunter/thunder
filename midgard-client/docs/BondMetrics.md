# BondMetrics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_active_bond** | **str** | Total bond of active nodes | [optional] 
**average_active_bond** | **str** | Average bond of active nodes | [optional] 
**median_active_bond** | **str** | Median bond of active nodes | [optional] 
**minimum_active_bond** | **str** | Minumum bond of active nodes | [optional] 
**maximum_active_bond** | **str** | Maxinum bond of active nodes | [optional] 
**total_standby_bond** | **str** | Total bond of standby nodes | [optional] 
**average_standby_bond** | **str** | Average bond of standby nodes | [optional] 
**median_standby_bond** | **str** | Median bond of standby nodes | [optional] 
**minimum_standby_bond** | **str** | Minumum bond of standby nodes | [optional] 
**maximum_standby_bond** | **str** | Maximum bond of standby nodes | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

