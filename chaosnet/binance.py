import argparse
from binance_chain.http import HttpApiClient
from binance_chain.messages import TransferMsg
from binance_chain.wallet import Wallet
from binance_chain.environment import BinanceEnvironment

parser = argparse.ArgumentParser(description='Arbitrager')
parser.add_argument("-k", "--key", type=str, required=True)
args = parser.parse_args()
env = BinanceEnvironment()
wallet = Wallet(private_key=args.key, env=env)
client = HttpApiClient()

transfer_msg = TransferMsg(
    wallet=wallet,
    symbol='BNB',
    amount=0.3,
    to_address='bnb1cx9qhluhtag3e5fsa5py5mx6j5zam22km72qep',
    memo='SWAP:BNB.RUNE-B1A'
)
res = client.broadcast_msg(transfer_msg, sync=True)
print(res)