import swagger_client
import logging
logging.basicConfig(
    #filename="debug.log",
    level=logging.DEBUG, format="%(asctime)s:%(levelname)s:%(message)s")
from swagger_client.rest import ApiException
from binance_chain.http import HttpApiClient
from binance_chain.messages import Transfer,TransferMsg
from binance_chain.wallet import Wallet


class ThorMonitor:
    def __init__(self):
        self.api_instance = swagger_client.DefaultApi()
        health = self.api_instance.get_health().catching_up
        logging.info("network health: %s" % health)

    def get_latest_txs(self, type=None):
        if type:
            latest = self.api_instance.get_tx_details(type=type, offset=0, limit=10)
            #txs_filtered = list(filter(lambda txs : txs.type == type, latest.txs))
            logging.info("latest txs with type %s: %s" % (type, latest))
        else:
            latest = self.api_instance.get_tx_details(offset=0, limit=10)
            logging.info("latest txs: %s" % latest)

    def get_stats(self):
        stats = self.api_instance.get_stats()
        logging.info("global stats: %s " % stats)

    def get_pools(self):
        pools = self.api_instance.get_pools()
        logging.info("available pools: %s " % pools)
        return pools

    def get_pools_detail(self, assets):
        pools_detail = self.api_instance.get_pools_details(asset=assets)
        logging.info("pools detail: %s" % pools_detail)

    def get_pools_address(self, chain=None):
        pools_address = self.api_instance.get_thorchain_proxied_endpoints()
        if chain:
            chain_filtered = list(filter(lambda current : current.chain == chain, pools_address.current))
            logging.info("chain: %s, address: %s" % (chain, chain_filtered[0].address))
        else:
            logging.info("pools address: %s" % pools_address)

    def get_network(self):
        network_info = self.api_instance.get_network_data()
        logging.info("network info: %s" % network_info)

    def get_nodes(self):
        node_info = self.api_instance.get_nodes()
        logging.info("node info: %s" % node_info)

    def get_constants(self):
        constant_info = self.api_instance.get_thorchain_proxied_constants()
        logging.info("constant info: %s" % constant_info)


class Settler:
    def __init__(self, key, symbol, amount, address, memo):
        self.client = HttpApiClient()
        self.transfer_to_thor = TransferMsg(
            wallet=Wallet(key),
            symbol=symbol,
            amount=amount,
            to_address=address,
            memo=memo
        )

    def swap(self):
        res = self.client.broadcast_msg(self.transfer_to_thor, sync=True)
        print(res)

    def stake(self):
        res = self.client.broadcast_msg(self.transfer_to_thor, sync=True)
        print(res)