from django.db import models


class Wallet(models.Model):
    COIN_CHOICES = [
        ('RUNE', 'RUNE'),
        ('BUSD', 'BUSD'),
        ('BNB', 'BNB'),
    ]
    virtual = models.BooleanField(default=True)
    wallet_address = models.CharField(max_length=43)
    tag = models.CharField(max_length=20)
    alpha_coin = models.CharField(
        max_length=10,
        choices=COIN_CHOICES,
        default='RUNE',
    )
    # alpha_coin_balance = models.CharField(
    #     max_length=10,
    #     default='0',
    # )
    beta_coin = models.CharField(
        max_length=10,
        choices=COIN_CHOICES,
        default='BNB',
    )
    # beta_coin_balance = models.CharField(
    #     max_length=10,
    #     default='0',
    # )
    theta_coin = models.CharField(
        max_length=10,
        choices=COIN_CHOICES,
        default='BUSD',
    )
    # theta_coin_balance = models.CharField(
    #     max_length=10,
    #     default='0',
    # )

    def __str__(self):
        return self.tag
