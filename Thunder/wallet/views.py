from django.http import HttpResponse
from django.views.generic import View
from django.shortcuts import render
from .models import Wallet
import sys
sys.path.append("..")
from src import Logger


class WalletView(View):
    # Render the first real wallet on default
    wallet = Wallet.objects.filter(virtual=False)[0]
    tokens = ['BNB', 'RUNE', 'BUSD']
    x = Logger(tokens, wallet.wallet_address)
    context = {
        'address': wallet.wallet_address,
        'alpha_coin': wallet.alpha_coin,
        'beta_coin': wallet.beta_coin,
        'theta_coin': wallet.theta_coin,
        'alpha_coin_balance': x.balances[wallet.alpha_coin],
        'beta_coin_balance': x.balances[wallet.beta_coin],
        'theta_coin_balance': x.balances[wallet.theta_coin],

    }

    def get(self, request):
        return render(request, 'wallet/index.html', self.context)


#class MonitorView(View):


