## Update
Upon the launch of ThorChain Chaosnet, this repo will be abandonded, a new version fully supporting chaosnet will be added to this official repo, this version only supports triangular arbitrage in binance dex.

## Usage
driver.py [-h] -k KEY -t TOKEN -b BASESIZE -o OMEGA
BASESIZE: in term of "TOKEN"
OMEGA: stands for the ratio between minimum liquidity and your basesize

## Current Bug:
Occasional API sequence number error

## General:
The creation of the bot is inspired both by ThorChain community (https://medium.com/@thorchain/rune-arb-bot-competition-478a72c86210) and my own school project (two school-related report can be found in docs/).
Shout out to entire ThorChain community, Kai, and Sonya for helping.

## Credits:
Used Python SDK(python-sdk) to broadcast message:
https://github.com/binance-chain/python-sdk/

Used swagger-editor to generate the rest of client-side api code(python-client):
https://github.com/swagger-api/swagger-editor

Learned Arbitrage here, will implement more sophisticated algo later on:
https://web.stanford.edu/~guillean/papers/uniswap_analysis.pdf

## WHAT WORKS:
- [ Import wallet keys locally ] 
- [ Compare markets, log trade opportunities ]
- [ Open and close orders to take trade opportunities ]
- [ Simple Testing ]
- [ Simple FrontEnd ]
- [ Auto parse triangular market pairs in binance dex]
- [ Two routes calculated for arbitraging triangular pairs]

## Requirement
python=3.5.6

## Install (Using Python3.5.6)
    git clone https://gitlab.com/zheye/thunder.git
    cd thunder
    
### Optional

    python3.5 -m venv env
    source env/bin/activate

### Install future-fstrings due to compatibility issue in python-sdk
    pip install future-fstrings
### Install swagger generated client side code (to switch to chaosnet partial support, use pip install --upgrade midgard-client
    pip install python-client/
### Install unofficial sdk for broadcasting msgs
    pip install python-sdk/

## Configure and Run
    driver.py [-h] -k KEY -t TOKEN -b BASESIZE -o OMEGA
    BASESIZE: in term of "TOKEN"
    OMEGA: stands for the ratio between minimum liquidity and your basesize

    ### FrontEnd (its a small demo so)
    cd Thunder
    python manage.py runserver
## Test
    cd test
    pytest *.py