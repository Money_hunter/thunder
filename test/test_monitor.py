import sys
import json
import pytest
sys.path.append("..")
from src import Monitor, LiquidityError


class TestMonitor:
    token = "RUNE-B1A"
    # Base Size = 2 RUNE
    tester = Monitor("wallet", 2 , 2)

    def test_initialization(self):
        self.tester.print_pairs()

    """ Test on input that results in profit for route 1 """
    def test_route1_profit(self):
        with open('monitor_test_suite/test_route1_profit/bnb_busd.json') as f:
            bnb_busd_pair = json.load(f)
        with open('monitor_test_suite/test_route1_profit/rune_bnb.json') as f:
            bnb_pair = json.load(f)
        with open('monitor_test_suite/test_route1_profit/rune_busd.json') as f:
            busd_pair = json.load(f)
        try:
            profit = self.tester.test_scan(self.token, busd_pair, bnb_pair, bnb_busd_pair)
            assert profit > 0
        except LiquidityError as e:
            print("Issue:  %s\n" % e)
            assert False

    """ Test on input that results in loss for route 1 """
    def test_route1_loss(self):
        with open('monitor_test_suite/test_route1_no_profit/bnb_busd.json') as f:
            bnb_busd_pair = json.load(f)
        with open('monitor_test_suite/test_route1_no_profit/rune_bnb.json') as f:
            bnb_pair = json.load(f)
        with open('monitor_test_suite/test_route1_no_profit/rune_busd.json') as f:
            busd_pair = json.load(f)
        with pytest.raises(LiquidityError, match="No Trade"):
            assert(self.tester.test_scan(self.token, busd_pair, bnb_pair, bnb_busd_pair))

    # def test_route1_liquidity(self):
    #     with open('monitor_test_suite/bnb_busd.json') as f:
    #         bnb_busd_pair = json.load(f)
    #     with open('monitor_test_suite/rune_bnb.json') as f:
    #         bnb_pair = json.load(f)
    #     with open('monitor_test_suite/rune_busd_no_liquid.json') as f:
    #         busd_pair = json.load(f)
    #     with pytest.raises(LiquidityError, match="Route 1 no liquidity"):
    #         assert(self.tester.test_scan(self.token, busd_pair, bnb_pair, bnb_busd_pair))

    # def test_route2(self):
    #     with open('monitor_test_suite/bnb_busd.json') as f:
    #         bnb_busd_pair = json.load(f)
    #     with open('monitor_test_suite/rune_bnb.json') as f:
    #         bnb_pair = json.load(f)
    #     with open('monitor_test_suite/rune_busd_r2.json') as f:
    #         busd_pair = json.load(f)
    #     try:
    #         profit = self.tester.test_scan(self.token, busd_pair, bnb_pair, bnb_busd_pair)
    #         assert profit > 0
    #     except LiquidityError as e:
    #         print("Issue:  %s\n" % e)
    #         assert False
    #
    # def test_route2_liquidity(self):
    #     with open('monitor_test_suite/bnb_busd.json') as f:
    #         bnb_busd_pair = json.load(f)
    #     with open('monitor_test_suite/rune_bnb.json') as f:
    #         bnb_pair = json.load(f)
    #     with open('monitor_test_suite/rune_busd_no_liquid_r2.json') as f:
    #         busd_pair = json.load(f)
    #     with pytest.raises(LiquidityError, match="Route 2 no liquidity"):
    #         assert(self.tester.test_scan(self.token, busd_pair, bnb_pair, bnb_busd_pair))
